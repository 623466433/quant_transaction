import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from "./en-US.js"
import zn from "./zh-CN.js"

Vue.use(VueI18n)

var lang = uni.getStorageSync('lang') || 'zn'
// if (!lang) {
// 	// 获取设备信息
// 	uni.getSystemInfo({
// 		success: function(res) {
// 			lang = res.language == 'zh-CN' ? 'zn' : 'en'
// 			uni.setStorageSync('lang', lang);
// 		}
// 	})
// }
const i18n = new VueI18n({
	locale: lang, // set locale  
	messages: {
		'zn': zn, // 中文语言包  
		'en': en // 英文语言包  
	},
	silentTranslationWarn: true
})

export default i18n
