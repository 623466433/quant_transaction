// 处理按钮在一定时间内被多次点击
function noMultipleClicks(methods) {
	let that = this;
	if (that.noClick) {
		that.noClick = false;
		methods();
		setTimeout(function() {
			that.noClick = true;
		}, 2000)
	} else {
		uni.showToast({
			duration: 1500,
			mask: true,
			icon: "none",
			title: "您已经签到成功了!"
		})
	}
}

//导出
export default {
	noMultipleClicks,
}
