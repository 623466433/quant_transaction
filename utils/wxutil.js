//登录提示弹窗
export function showLoginDialog() {
	//#ifdef  MP-WEIXIN
	uni.showModal({
		title: '提示',
		content: '不授权则无法进行更多操作，点击去授权按钮前往授权',
		confirmText: '去授权',
		success: res => {
			let {
				confirm
			} = res;

			if (confirm) {
				uni.navigateTo({
					url: '/pages/login/login'
				});
			}
		}
	});
	// #endif
} // 微信支付

export function wxpay(opt) {
	return new Promise((resolve, reject) => {
		uni.requestPayment({
			timeStamp: opt.timeStamp,
			// 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
			nonceStr: opt.nonceStr,
			// 支付签名随机串，不长于 32 位
			package: opt.package,
			// 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
			signType: opt.signType,
			// 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
			paySign: opt.paySign,
			// 支付签名
			success: res => {
				resolve();
			},
			cancel: res => {
				reject();
			},
			fail: res => {
				reject();
			}
		});
	});
}
